<?php

namespace Tests\AppBundle\Controller\Localization;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class CountryControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        // $this->loadFixtureFiles(array('@AppBundle/DataFixtures/ORM/Localization.Country.yml'));
    }


    public function testList(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_localization_country_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
