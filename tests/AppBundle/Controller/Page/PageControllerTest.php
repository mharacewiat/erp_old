<?php

namespace Tests\AppBundle\Controller\Page;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class PageControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->loadFixtureFiles(array('@AppBundle/DataFixtures/ORM/Page.Page.yml'));
    }


    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_page_page_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
