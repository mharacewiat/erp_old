<?php

namespace Tests\AppBundle\Controller\Document;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class OfferControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->loadFixtureFiles(array('@AppBundle/DataFixtures/ORM/Document.Offer.yml'));
    }


    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_document_offer_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
