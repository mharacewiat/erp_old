<?php

namespace Tests\AppBundle\Controller\Warehouse;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class WarehouseControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->loadFixtureFiles(array('@AppBundle/DataFixtures/ORM/Warehouse.Warehouse.yml'));
    }


    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_warehouse_warehouse_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
