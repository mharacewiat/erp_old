<?php

namespace Tests\AppBundle\Controller\Project;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class ProjectControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->loadFixtureFiles(array(
            '@AppBundle/DataFixtures/ORM/Project.Project.yml',
            '@AppBundle/DataFixtures/ORM/Project.Task.yml',
        ));
    }


    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_project_project_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
