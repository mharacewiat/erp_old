<?php

namespace Tests\AppBundle\Controller\Employee;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class ContractControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->loadFixtureFiles(array('@AppBundle/DataFixtures/ORM/Employee.Contract.yml'));
    }


    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_employee_contract_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
