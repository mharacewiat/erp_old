<?php

namespace Tests\AppBundle\Controller\User;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class RoleControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->loadFixtureFiles(array(
            '@AppBundle/DataFixtures/ORM/User.Role.yml',
            '@AppBundle/DataFixtures/ORM/User.Group.yml',
            '@AppBundle/DataFixtures/ORM/User.User.yml',
        ));
    }


    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_user_role_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
