<?php

namespace Tests\AppBundle\Controller\Contractor;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class ContractorControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->loadFixtureFiles(array('@AppBundle/DataFixtures/ORM/Contractor.Contractor.yml'));
    }


    public function testList(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_contractor_contractor_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
