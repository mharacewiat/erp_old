<?php

namespace Tests\AppBundle\Controller\Product;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{

    public function setUp(): void
    {
        parent::setUp();
        $this->loadFixtureFiles(array(
            '@AppBundle/DataFixtures/ORM/Product.Product.yml',
            '@AppBundle/DataFixtures/ORM/Product.Taxonomy.yml',
        ));
    }


    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', $client->getContainer()->get('router')->generate('app_product_product_list'));

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Gentelella Alela!', $crawler->filter('.main_container')->text());
    }

}
