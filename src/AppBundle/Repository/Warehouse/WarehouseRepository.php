<?php

namespace AppBundle\Repository\Warehouse;

use Doctrine\ORM\EntityRepository;

/**
 * WarehouseRepository class.
 *
 * @package AppBundle\Repository\Warehouse
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class WarehouseRepository extends EntityRepository
{

}
