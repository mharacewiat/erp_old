<?php

namespace AppBundle\Repository\Document;

use Doctrine\ORM\EntityRepository;

/**
 * OfferRepository class.
 *
 * @package AppBundle\Repository\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class OfferRepository extends EntityRepository
{

}
