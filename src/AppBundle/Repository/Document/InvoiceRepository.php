<?php

namespace AppBundle\Repository\Document;

use Doctrine\ORM\EntityRepository;

/**
 * InvoiceRepository class.
 *
 * @package AppBundle\Repository\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class InvoiceRepository extends EntityRepository
{

}
