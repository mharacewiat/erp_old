<?php

namespace AppBundle\Repository\Document;

use Doctrine\ORM\EntityRepository;

/**
 * OrderRepository class.
 *
 * @package AppBundle\Repository\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class OrderRepository extends EntityRepository
{

}
