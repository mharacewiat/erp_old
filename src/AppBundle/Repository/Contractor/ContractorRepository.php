<?php

namespace AppBundle\Repository\Contractor;

use Doctrine\ORM\EntityRepository;

/**
 * ContractorRepository class.
 *
 * @package AppBundle\Repository\Contractor
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class ContractorRepository extends EntityRepository
{

}
