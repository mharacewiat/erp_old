<?php

namespace AppBundle\Repository\Page;

use Doctrine\ORM\EntityRepository;

/**
 * PageRepository class.
 *
 * @package AppBundle\Repository\Page
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class PageRepository extends EntityRepository
{

}
