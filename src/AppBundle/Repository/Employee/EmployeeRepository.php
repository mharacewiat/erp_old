<?php

namespace AppBundle\Repository\Employee;

use Doctrine\ORM\EntityRepository;

/**
 * EmployeeRepository class.
 *
 * @package AppBundle\Repository\Employee
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class EmployeeRepository extends EntityRepository
{

}
