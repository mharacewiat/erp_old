<?php

namespace AppBundle\Repository\Product;

use Doctrine\ORM\EntityRepository;

/**
 * TaxonomyRepository class.
 *
 * @package AppBundle\Repository\Product
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class TaxonomyRepository extends EntityRepository
{

}
