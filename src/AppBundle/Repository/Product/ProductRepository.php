<?php

namespace AppBundle\Repository\Product;

use Doctrine\ORM\EntityRepository;

/**
 * ProductRepository class.
 *
 * @package AppBundle\Repository\Product
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class ProductRepository extends EntityRepository
{

}
