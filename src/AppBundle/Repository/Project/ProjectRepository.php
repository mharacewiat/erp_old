<?php

namespace AppBundle\Repository\Project;

use Doctrine\ORM\EntityRepository;

/**
 * ProjectRepository class.
 *
 * @package AppBundle\Repository\Project
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class ProjectRepository extends EntityRepository
{

}
