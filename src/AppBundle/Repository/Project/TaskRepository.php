<?php

namespace AppBundle\Repository\Project;

use Doctrine\ORM\EntityRepository;

/**
 * TaskRepository class.
 *
 * @package AppBundle\Repository\Project
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class TaskRepository extends EntityRepository
{
}
