<?php

namespace AppBundle\Repository\User;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository class.
 *
 * @package AppBundle\Repository\User
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class UserRepository extends EntityRepository
{

}
