<?php

namespace AppBundle\Repository\User;

use Doctrine\ORM\EntityRepository;

/**
 * RoleRepository class.
 *
 * @package AppBundle\Repository\User
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class RoleRepository extends EntityRepository
{

}
