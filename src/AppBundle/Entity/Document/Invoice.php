<?php

namespace AppBundle\Entity\Document;

/**
 * Invoice class.
 *
 * @package AppBundle\Entity\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Invoice
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
