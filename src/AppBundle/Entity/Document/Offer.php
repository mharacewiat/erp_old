<?php

namespace AppBundle\Entity\Document;

/**
 * Offer class.
 *
 * @package AppBundle\Entity\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Offer
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
