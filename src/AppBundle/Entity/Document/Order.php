<?php

namespace AppBundle\Entity\Document;

/**
 * Order class.
 *
 * @package AppBundle\Entity\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Order
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
