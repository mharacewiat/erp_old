<?php

namespace AppBundle\Entity\Project;

/**
 * Project class.
 *
 * @package AppBundle\Entity\Project
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Project
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
