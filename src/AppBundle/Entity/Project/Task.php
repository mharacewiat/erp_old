<?php

namespace AppBundle\Entity\Project;

/**
 * Task class.
 *
 * @package AppBundle\Entity\Project
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Task
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var Project
     */
    protected $project;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Project|null $project
     * @return $this
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

}
