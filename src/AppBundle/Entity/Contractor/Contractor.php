<?php

namespace AppBundle\Entity\Contractor;

/**
 * Contractor class.
 *
 * @package AppBundle\Entity\Contractor
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Contractor
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Contractor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
