<?php

namespace AppBundle\Entity\Employee;

/**
 * Contract class.
 *
 * @package AppBundle\Entity\Employee
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Contract
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
