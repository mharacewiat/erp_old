<?php

namespace AppBundle\Entity\Employee;

/**
 * Employee class.
 *
 * @package AppBundle\Entity\Employee
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Employee
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
