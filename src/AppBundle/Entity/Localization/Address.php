<?php

namespace AppBundle\Entity\Localization;

/**
 * Address class.
 *
 * @package AppBundle\Entity\Localization
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Address
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $firstLine;

    /**
     * @var string
     */
    protected $secondLine;

    /**
     * @var City
     */
    protected $city;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstLine
     *
     * @param string $firstLine
     *
     * @return Address
     */
    public function setFirstLine($firstLine)
    {
        $this->firstLine = $firstLine;

        return $this;
    }

    /**
     * Get firstLine
     *
     * @return string
     */
    public function getFirstLine()
    {
        return $this->firstLine;
    }

    /**
     * Set secondLine
     *
     * @param string $secondLine
     *
     * @return Address
     */
    public function setSecondLine($secondLine)
    {
        $this->secondLine = $secondLine;

        return $this;
    }

    /**
     * Get secondLine
     *
     * @return string
     */
    public function getSecondLine()
    {
        return $this->secondLine;
    }

    /**
     * Set city
     *
     * @param City $city
     *
     * @return Address
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }
}
