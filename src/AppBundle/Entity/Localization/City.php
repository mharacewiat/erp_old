<?php

namespace AppBundle\Entity\Localization;

/**
 * City class.
 *
 * @package AppBundle\Entity\Localization
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class City
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $latitude;

    /**
     * @var string
     */
    protected $longitude;


    /**
     * @var Country
     */
    protected $capital;


    /**
     * @var Region
     */
    protected $region;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return City
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return City
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }


    /**
     * Set capital
     *
     * @param Country $capital
     *
     * @return City
     */
    public function setCapital(Country $capital = null)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return Country
     */
    public function getCapital()
    {
        return $this->capital;
    }


    /**
     * Set region
     *
     * @param Region $region
     *
     * @return City
     */
    public function setRegion(Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

}
