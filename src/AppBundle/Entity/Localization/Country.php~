<?php

namespace AppBundle\Entity\Localization;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Country class.
 *
 * @package AppBundle\Entity\Localization
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Country
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;


    /**
     * @var City
     */
    protected $capital;


    /**
     * @var Collection
     */
    protected $regions;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->regions = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set capital
     *
     * @param City $capital
     *
     * @return Country
     */
    public function setCapital(City $capital = null)
    {
        $this->capital = $capital;

        return $this;
    }

    /**
     * Get capital
     *
     * @return City
     */
    public function getCapital()
    {
        return $this->capital;
    }


    /**
     * Add region
     *
     * @param Region $region
     *
     * @return Country
     */
    public function addRegion(Region $region)
    {
        $this->regions[] = $region;

        return $this;
    }

    /**
     * Remove region
     *
     * @param Region $region
     */
    public function removeRegion(Region $region)
    {
        $this->regions->removeElement($region);
    }

    /**
     * Get regions
     *
     * @return Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }

}
