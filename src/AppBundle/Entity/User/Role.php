<?php

namespace AppBundle\Entity\User;

/**
 * Role class.
 *
 * @package AppBundle\Entity\User
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Role
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}
