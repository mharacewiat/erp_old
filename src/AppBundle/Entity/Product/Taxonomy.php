<?php

namespace AppBundle\Entity\Product;

/**
 * Taxonomy class.
 *
 * @package AppBundle\Entity\Product
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Taxonomy
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
