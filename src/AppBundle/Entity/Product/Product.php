<?php

namespace AppBundle\Entity\Product;

/**
 * Product class.
 *
 * @package AppBundle\Entity\Product
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Product
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
