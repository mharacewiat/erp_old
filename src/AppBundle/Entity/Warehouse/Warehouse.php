<?php

namespace AppBundle\Entity\Warehouse;

/**
 * Warehouse class.
 *
 * @package AppBundle\Entity\Warehouse
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class Warehouse
{

    /**
     * @var int
     */
    protected $id;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var string
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Warehouse
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
