<?php

namespace AppBundle\Controller\Product;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * TaxonomyController class.
 *
 * @package AppBundle\Controller\Product
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class TaxonomyController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/product/taxonomy/list.html.twig', array(
            // ...
        ));
    }

}
