<?php

namespace AppBundle\Controller\Product;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * ProductController class.
 *
 * @package AppBundle\Controller\Product
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class ProductController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/product/product/list.html.twig', array(
            // ...
        ));
    }

}
