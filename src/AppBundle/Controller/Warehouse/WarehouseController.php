<?php

namespace AppBundle\Controller\Warehouse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * WarehouseController class.
 *
 * @package AppBundle\Controller\Warehouse
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class WarehouseController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/warehouse/warehouse/list.html.twig', array(
            // ...
        ));
    }

}
