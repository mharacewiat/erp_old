<?php

namespace AppBundle\Controller\Page;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * PageController class.
 *
 * @package AppBundle\Controller\Page
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class PageController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/page/page/list.html.twig', array(
            // ...
        ));
    }

}
