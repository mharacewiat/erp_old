<?php

namespace AppBundle\Controller\Project;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * TaskController class.
 *
 * @package AppBundle\Controller\Project
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class TaskController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/project/task/list.html.twig', array(
            // ...
        ));
    }

}
