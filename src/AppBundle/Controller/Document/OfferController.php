<?php

namespace AppBundle\Controller\Document;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * OfferController class.
 *
 * @package AppBundle\Controller\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class OfferController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/document/offer/list.html.twig', array(
            // ...
        ));
    }

}
