<?php

namespace AppBundle\Controller\Document;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * OrderController class.
 *
 * @package AppBundle\Controller\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class OrderController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/document/order/list.html.twig', array(
            // ...
        ));
    }

}
