<?php

namespace AppBundle\Controller\Document;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * InvoiceController class.
 *
 * @package AppBundle\Controller\Document
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class InvoiceController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/document/invoice/list.html.twig', array(
            // ...
        ));
    }

}
