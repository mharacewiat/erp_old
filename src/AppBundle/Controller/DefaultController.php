<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * DefaultController class.
 *
 * @package AppBundle\Controller
 * @author Michał Haracewiat <michal.haracewiat@polcode.net>
 */
class DefaultController extends Controller
{

    /**
     * @return Response
     */
    public function indexAction(): Response
    {
        // replace this example code with whatever you need
        return $this->render('app/default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

}
