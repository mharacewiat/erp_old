<?php

namespace AppBundle\Controller\Localization;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * RegionController class.
 *
 * @package AppBundle\Controller\Localization
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class RegionController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/localization/region/list.html.twig', array(
            // ...
        ));
    }

}
