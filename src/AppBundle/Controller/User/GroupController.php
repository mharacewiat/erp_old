<?php

namespace AppBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * GroupController class.
 *
 * @package AppBundle\Controller\User
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class GroupController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/user/group/list.html.twig', array(
            // ...
        ));
    }

}
