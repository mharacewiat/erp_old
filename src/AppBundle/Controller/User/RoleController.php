<?php

namespace AppBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * RoleController class.
 *
 * @package AppBundle\Controller\User
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class RoleController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/user/role/list.html.twig', array(
            // ...
        ));
    }

}
