<?php

namespace AppBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * UserController class.
 *
 * @package AppBundle\Controller\User
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class UserController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/user/user/list.html.twig', array(
            // ...
        ));
    }

}
