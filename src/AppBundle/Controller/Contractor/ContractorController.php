<?php

namespace AppBundle\Controller\Contractor;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * ContractorController class.
 *
 * @package AppBundle\Controller\Contractor
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class ContractorController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/contractor/contractor/list.html.twig', array(
            // ...
        ));
    }

}
