<?php

namespace AppBundle\Controller\Employee;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * ContractController class.
 *
 * @package AppBundle\Controller\Employee
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class ContractController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/employee/contract/list.html.twig', array(
            // ...
        ));
    }

}
