<?php

namespace AppBundle\Controller\Employee;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * EmployeeController class.
 *
 * @package AppBundle\Controller\Employee
 * @author Michał Haracewiat <haruni000@gmail.com>
 */
class EmployeeController extends Controller
{

    /**
     * @return Response
     */
    public function listAction(): Response
    {
        return $this->render('app/employee/employee/list.html.twig', array(
            // ...
        ));
    }

}
